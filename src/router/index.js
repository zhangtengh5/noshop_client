import Vue from 'vue'
import Router from 'vue-router'
import store from '@/vuex/store';
import { getQueryString } from '@/assets/js/handleUrl.js'
import goods from '@/components/goods'
import result from '@/components/result'
import success from '@/components/success'
import fail from '@/components/fail'
import test1 from '@/components/test1'
Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '/goods',
      name: 'goods',
      component: goods
    },
    {
      path: '/result',
      name: 'result',
      component: result
    },
    {
      path: '/success',
      name: 'success',
      component: success
    },
    {
      path: '/fail',
      name: 'fail',
      component: fail
    },
    {
      path: '/test',
      name: 'test',
      component: test1
    }
  ]
})
router.beforeEach((to, from, next) => {
  store.state.equipmentID = getQueryString('equipmentID');
  if (from.path === '/goods' || '/success' || '/fail') {
    clearInterval(store.state.timer);
  }
  if (from.path === '/result') {
    clearInterval(store.state.payTimer);
    store.commit('cancleSoft');
  }
  if (from.path !== '/') {
    if (to.path === '/') {
      window.external.clearEpc();
      sessionStorage.removeItem('bag')
      sessionStorage.removeItem('codeGoods')
      store.state.goodsLists = [];
      store.state.goodsCodeLists = [];
      store.state.goodsBags = [];
      store.state.rfid = '';
      store.state.isOrder = false;
    }
  }
  next();
})
export default router