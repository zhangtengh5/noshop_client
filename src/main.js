// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import axios from './axios/http';
import store from './vuex/store';
import {Notification, Message} from 'element-ui';
import VueAwesomeSwiper from 'vue-awesome-swiper';
Vue.config.productionTip = false;
// require styles
import 'swiper/dist/css/swiper.css';
import './assets/scss/base.scss';
Vue.use(VueAwesomeSwiper);
Vue.prototype.$notify = Notification;
Vue.prototype.$message = Message;
// 将axios挂载到prototype上，在组件中可以直接使用this.axios访问
Vue.prototype.axios = axios;
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  axios,
  store,
  components: { App },
  template: '<App/>'
})
