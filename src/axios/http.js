import axios from 'axios'
import qs from 'qs';
//请求超时时限
axios.defaults.timeout =  20000;
axios.defaults.baseURL = process.env.NODE_ENV === 'production' ? '/' : '/api';
// http request 拦截器
axios.interceptors.request.use(
  config => {
    if (config.method=="post"){
        config.data = qs.stringify(config.data);
        config.headers['Content-Type'] = 'application/x-www-form-urlencoded';
    }
    return config;
  },
  error => {
    error.message = '数据请求出现异常'
    return Promise.reject(error);
  });

// http response 拦截器
axios.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    if (error && error.response) {
      switch (error.response.status) {
          case 400: error.message = '请求错误(400),请重试';break;
          case 401: error.message = '(401),请重试';break;
          case 403: error.message = '拒绝访问(403),请重试';break;
          case 404: error.message = '请求出错(404),请重试';break;
          case 408: error.message = '请求超时(408),请重试'; break;
          case 500: error.message = '服务器错误(500),请重试'; break;
          case 501: error.message = '服务未实现(501),请重试'; break;
          case 502: error.message = '网络错误(502)，请重试'; break;
          case 503: error.message = '服务不可用(503),请重试'; break;
          case 504: error.message = '网络超时(504),请重试'; break;
          case 505: error.message = 'HTTP版本不受支持(505),请重试'; break;
          default: error.message = `连接出错(${error.response.status}),请重试`;
      }
    } else {
      error.message = '网络不稳定,请重试'
    }
    return Promise.reject(error);
  });

export default axios;