import Vuex from 'vuex'
import Vue from 'vue'
import router from '../router';
import { Message } from 'element-ui';
import axios from '../axios/http';
Vue.use(Vuex);
export default new Vuex.Store({
    state: {
        allQuantity: 0,
        allPrice: 0,
        totalDiscounts: 0,
        totalAll: 0,
        timer: null,
        payTimer: null,
        activexObj: null,
        goodsLists: [],
        goodsCodeLists: [],
        goodsBags: [],
        statusInfo: null,
        orderData: null,
        handPay: null,
        message: '',
        isOrder: false,
        rfid: '',
        equipmentID: null,
        isShowLoading: false,
        title: '正在加载中...',
        reTime: 0,
        oldreTime: 3
    },
    mutations: {
        computedGoods(state, payload) {
            //state.allQuantity = 0;
            //state.allPrice = 0;
            state.goodsBags = [];
            let bag = sessionStorage.getItem('bag');
            if (bag) {
              state.goodsBags.push(JSON.parse(bag));
            }
            /*
            let data = state.goodsLists.concat(state.goodsCodeLists, state.goodsBags);
            for (let i = 0; i < data.length; i++) {
              if (parseInt(data[i].isBuy) === 0) {
                state.allQuantity += data[i].count;
                state.allPrice += (data[i].count * data[i].price);
              }
            }
            */
            let codeList = state.goodsCodeLists.concat(state.goodsBags);
            state.isShowLoading = true;
            state.title = '校验掌静脉数据中...';
            axios.post('/settlement/CalculationMoney.do', {
              rfid: state.rfid,
              equipmentName: state.equipmentID,
              codeList: JSON.stringify(codeList)
            }).then((response) => {
                let result_data = response.data;
                state.isShowLoading = false;
                if (parseInt(result_data.status) === 1) {
                    //开始支付
                    state.allQuantity = result_data.count;
                    state.totalAll = result_data.xiaoji;
                    state.totalDiscounts = result_data.manjian;
                    state.allPrice = result_data.xiaoji - result_data.manjian;
                } else {
                  Message({
                    message: result_data.msg,
                    duration: 1500,
                    type: 'warning'
                  });
                }
                router.push('/goods');
            }).catch((error)=> {
                state.isShowLoading = false;
                Message({
                    message: error.message,
                    duration: 1500,
                    type: 'error'
                });
            })
        },
        goPayment (state, payload) {
          state.orderData = payload.orderData;
          router.push('/result');
          state.activexObj.capturePalmData();
        },
        cancleSoft (state, payload) {
          state.activexObj.cancleCapturePalm();//取消验证
        },
        againPay (state, payload) {
            state.orderData = payload.orderData;
            router.push('/result');
            state.activexObj.capturePalmData();//又开始支付
        },
        registStatus (state, payload) {
            //状态信息
            state.statusInfo = JSON.parse(payload.statusInfo);
        },
        capturePalmData (state, payload) {
            //验证接口
            let verifyData = payload.verifyData;
            state.isShowLoading = true;
            state.title = '校验掌静脉数据中...';
            axios.post('/vein/find.do', {
                veinData: verifyData,
                equipmentName: state.equipmentID
            }).then((response) => {
                let result_data = response.data;
                state.isShowLoading = false;
                if(parseInt(result_data.result) === 0) {
                    //开始支付
                    let handPay = state.handPay;
                    let userId = result_data.id;
                    let orderId = state.orderData.data.id;
                    handPay(userId, orderId);
                    state.reTime = 0;
                } else {
                    //支付失败/没有注册过
                    state.reTime++;
                    if (state.reTime > state.oldreTime) {
                        state.reTime = 0;
                        clearInterval(state.payTimer);
                        state.message = '掌静脉识别失败，请重新支付';
                        router.push('/fail');
                    } else {
                      state.activexObj.capturePalmData(); //又开始支付
                    }
                }
            }).catch((error)=> {
                state.isShowLoading = false;
                Message({
                    message: error.message,
                    duration: 1500,
                    type: 'error'
                });
            })
        }
    },
    actions: {
        checkLocal({commit,state,dispatch}, payload) {
          let data = payload.data;
          if (sessionStorage.getItem('codeGoods')) {
            let localData = JSON.parse(sessionStorage.getItem('codeGoods'));
            let index = NaN;
            for (let i = 0; i < localData.length; i++) {
              if (data.proId === localData[i].proId) {
                index = i;
              }
            }
            if (!isNaN(index)) {
              localData[index].count++;
            } else {
              localData.push(data);
            }
            state.goodsCodeLists = localData;
          } else {
            state.goodsCodeLists.unshift(data);
          }
          sessionStorage.setItem('codeGoods', JSON.stringify(state.goodsCodeLists))
          commit('computedGoods')
        },
        clickLocal ({commit, state, dispatch}, payload) {
            let data = payload.data;
            if (sessionStorage.getItem('codeGoods')) {
              let localData = JSON.parse(sessionStorage.getItem('codeGoods'));
              let index = NaN;
              for (let i = 0; i < localData.length; i++) {
                if (data.proId === localData[i].proId) {
                  index = i;
                }
              }
              if (!isNaN(index)) {
                if (data.count === 0) {
                  localData.splice(index, 1);
                } else {
                  localData[index] = data;
                }
              } else {
                localData.push(data);
              }
              state.goodsCodeLists = localData;
            } else {
              state.goodsCodeLists.push(data);
            }
            sessionStorage.setItem('codeGoods', JSON.stringify(state.goodsCodeLists))
            commit('computedGoods')
        },
        sendQRCode({ commit, state, dispatch}, payload) {
          let code = payload.code;
          //let code = codestr.replace(/\r|\n/g, '');
          state.isShowLoading = true;
          state.title = '提交数据中...';
          let equipmentID = state.equipmentID;
          axios.post('/settlement/CreateShopIvCode.do', {
            code: code,
            equipmentName: equipmentID
          }).then((response) => {
            let result_data = response.data;
            state.isShowLoading = false;
            if (parseInt(result_data.status) === 1) {
              let data = result_data.data;
              data.Ivcode = code;
              dispatch({
                type: 'checkLocal',
                data: data
              })
            } else {
              Message({
                message: result_data.msg,
                type: 'warning',
                duration: 1500
              });
            }
          }).catch((error) => {
            state.isShowLoading = false;
            Message({
              message: error.message,
              type: 'error',
              duration: 1500
            });
          })
        },
        sendRFID({ commit, state}, payload) {
          let rfid = payload.rfid;
          state.rfid = rfid;
          state.isShowLoading = true;
          state.title = '提交数据中...';
          let equipmentID = state.equipmentID;
          axios.post('/settlement/CreateShopRFID.do', {
            rfid: rfid,
            equipmentName: equipmentID
          }).then((response) => {
            let result_data = response.data;
            state.isShowLoading = false;
            if (parseInt(result_data.status) === 1) {
              let data = result_data.data;
              if (data.length > 0) {
                state.goodsLists = data;
                commit('computedGoods')
              } else {
                //没有读取到有效商品重置rfid
                Message({
                  message: '请扫描有效的商品',
                  type: 'warning',
                  duration: 1500
                });
                window.external.clearEpc();
              }
            } else {
              Message({
                message: result_data.msg,
                type: 'warning',
                duration: 1500
              });
              window.external.clearEpc();
            }
          }).catch((error) => {
            state.isShowLoading = false;
            window.external.clearEpc();
            Message({
              message: error.message,
              type: 'error',
              duration: 1500
            });
          })
        }
    }
})