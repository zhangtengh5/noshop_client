
export function addClass(obj, cls){
    var obj_class = obj.className; //获取 class 内容.
    var blank = (obj_class != '') ? ' ' : ''; //判断获取到的 class 是否为空, 如果不为空在前面加个'空格'.
    var added = obj_class + blank + cls; //组合原来的 class 和需要添加的 class.
    obj.className = added; //替换原来的 class.
}
    
export function removeClass(obj, cls) {
    var obj_class = ' '+obj.className+' '; //获取 class 内容, 并在首尾各加一个空格. ex) 'abc    bcd' -> ' abc    bcd '
    obj_class = obj_class.replace(/(\s+)/gi, ' '); //将多余的空字符替换成一个空格. ex) ' abc    bcd ' -> ' abc bcd '
    var removed = obj_class.replace(' '+cls+' ', ' '); //在原来的 class 替换掉首尾加了空格的 class. ex) ' abc bcd ' -> 'bcd '
    removed = removed.replace(/(^\s+)|(\s+$)/g, ''); //去掉首尾空格. ex) 'bcd ' -> 'bcd'
    obj.className = removed;//替换原来的 class.
}
    
export function hasClass(obj, cls) {
  var obj_class = obj.className; //获取 class 内容.
  var obj_class_lst = obj_class.split(/\s+/);//通过split空字符将cls转换成数组.
  var x = 0;
  for(x in obj_class_lst) {
    if(obj_class_lst[x] == cls) { //循环数组, 判断是否包含cls
      return true;
    }
  }
  return false;
}

export function getDocument (param, obj) {
  var obj = obj || document //  没有传递 obj 参数，则默认使用 document
  if (param.charAt(0) === "#") {  //  通过id名获取元素
    return document.getElementById(param.slice(1)) //  这里的slice是字符串的一个函数，用于获取第2个元素及之后的字符串
  }      
  if (param.indexOf(".") === 0) {
    return getByClass(param.slice(1), obj) //  调用下面的getClass函数
  }
  return obj.getElementsByTagName(param) //  tag
}

function getByClass (className, obj) {
  if (obj.getElementsClassName) {
    return obj.getElementsByClassName(className) 
  } else {
    let result = []
    let tags = obj.getElementsByTagName("*")
    for (let i = 0; i < tags.length; i++) {
      //  获取到当前遍历元素所使用的所有类名
      let classNames = tags[i].className.split(" ")
      //  遍历当前元素的每个类名
      for (let j = 0; j < classNames.length; j++) { 
        if (classNames[j] === className) { //  说明当前遍历到的元素使用过要查找的类名
          result.push(tags[i])
        }
      }
    }
    return result
  }
}