var express = require('express');
var router = express.Router();
var path = require('path');
router.get('/', (req, res, next) => {
    var paths = path.join(__dirname, '..', 'dist/index.html')
    res.sendFile(paths)
})
module.exports = router;